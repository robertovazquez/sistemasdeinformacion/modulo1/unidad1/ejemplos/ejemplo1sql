﻿--Ejemplo de creacion de una base de datos de veterinarios
--Tiene 3 tablas
DROP DATABASE IF EXISTS b20190528;
CREATE DATABASE b20190528;
USE b20190528;
/*
  creando la tabla animales
*/
CREATE TABLE animales (
  id int AUTO_INCREMENT,
  nombre varchar(100),
  raza varchar(100),
  fechaNac date,
  PRIMARY KEY(id)
);

CREATE TABLE veterinarios (
cod int AUTO_INCREMENT,
nombre varchar (100),
especialidad varchar (100),
PRIMARY KEY (cod) -- creando la clave
);
CREATE TABLE acuden(
idani int,
codvete int,
fecha date,
PRIMARY KEY (idani, codvete, fecha),
UNIQUE KEY (idani),
CONSTRAINT fkacudenanimal FOREIGN KEY(idani)
  REFERENCES animales(id),
CONSTRAINT fkacudenveterinario FOREIGN KEY(codvete)
  REFERENCES veterinarios(cod)


);
--insertar un registro

INSERT INTO animales (nombre, raza, fechaNac)
  VALUES ('jorge','bulldog','2000/2/1'),
  ('ana','caniche','2002/1/2');

--listar

SELECT * FROM animales a;


 